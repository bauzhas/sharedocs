### This tutorial will show how to, step by step, setup Arch Linux Arm and configure Kodi on a Raspberry Pi 3 Model B+. Your host computer must be running Linux for this tutorial.
---
### Prerequisites:

- Raspberry Pi 3 Model B
- 5V 2.1A Micro-usb Power Supply
- Micro sdcard (I'd recommend at least 16GB)
- USB Keyboard (for initial setup)
- HDMI Cablehttps://gist.github.com/adcar/f2a61c942a878757719b3cb2bc233d38
---
### Step 1: Setup Arch Linux Arm

1. Open up a terminal and run `lsblk`.
2. Insert your Micro sdcard into your computer.
3. Run `lsblk` once again. You should now see a new device (which is probably named mmcblk0). This is your sdcard.
4. Keep note of what your sdcard is called, as we will be using it throughout this tutorial. (I will refer to it as mmcblkY in commands as an example)
5. Run `sudo -i` to drop to a root shell.
6. Now run `fdisk /dev/mmcblkY`

You should now be brought to an fdisk prompt You will need to enter a series of commands from here.\
[- Warning: If you mess up any part of this (e.g., hit the wrong button) then hit Ctrl-C to cancel (it will not write changes unless you hit w) -]

1. Type `o` to clear partitions.
2. Optionally type `p` to list partitons and make sure there aren't any left.
3. Type `n` for new partition, and type `p` for primary, then `1` for first partiton. Hit `enter` to accept the default first sector and then type `+100M` for the last sector.
4. Type `t` for partition type, then `c` to set the first partition to FAT32.
5. Type `n` for new partition, then `p` and `2` for the second partition on your sdcard. Hit `enter` twice to accept the first and last sector.
6. Type `w` to write all of your changes to your sdcard.
7. Now all your partitons are set up!
8. Run `lsblk`. You should see two seperate partitons, one called something like mmcblkYp1 and one called mmcblkYp2. These are your two partitons. The first will be FAT32 (for booting) and the second will be EXT4 (for the root filesystem).

Next, enter the following commands to create and mount the FAT32 filesystem (the first partition):
```shell
mkfs.vfat /dev/mmcblkYp1
```
```shell
mkdir boot
```
```shell
mount /dev/mmcblkYp1 boot
```
Then, enter the following commands to create and mount the EXT4 filesystem (the second partition):
```shell
mkfs.ext4 /dev/sdX2
```
```shell
mkdir root
```
```shell
mount /dev/sdX2 root
```
Now, we can download Arch Linux ARM filesystem and extract it to the root directory we made earlier:
```shell
curl -L -O http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-2-latest.tar.gz
```
```shell
bsdtar -xpf ArchLinuxARM-rpi-2-latest.tar.gz -C root
```
Optionally, you could substitue ArchLinuxARM-rpi-2-latest.tar.gz for ArchLinuxARM-rpi-3-latest.tar.gz for an AArch64 Installation (which is more unstable, but faster). Both work on Raspberry Pi 3.

Now run `sync` to make sure everything is properly synced.
Now move the boot files from root/boot into the boot partition/directory.
```shell
mv root/boot/* boot
```
Now we can unmount the both of the partitions/directories:
```shell
umount boot root
```
Arch Linux is now installed on your sdcard.

---
### Step 2: Configure Arch Linux:

Set up uk keyboard layout enterin uk layout in the /etc/vconsole.conf file:
```shell
vim /etc/vconsole.conf
```
```shell
KEYMAP=uk
```
Eject the sdcard from your computer and insert it into your Raspberry Pi.\
Plug in the Pi with HDMI to your TV / monitor. Now plug in the Pi with your 5V 2.1A Micro-usb Power Supply. The Pi should start up on its own, and you should be brought to a terminal screen.\
Go ahead and plug in a keyboard to your Pi and enter root for the username and root for the password.\
The first thing you should do is change the root password. Enter `passwd` and type in a new password.

Now create a new user (replace exampleusername with whatever username you want):
```shell
useradd -m -g users -G wheel,storage,power -s /bin/bash exampleusername
```
Now set a password for the newly created user with 
```shell
passwd exampleusername
```
Now let's set up Wi-Fi. Run `wifi-menu` to bring up a ncurses menu. You can then select your Wi-Fi network and enter the password. Now upgrade your system and install sudo:
```shell
pacman-key --init
```
```shell
pacman-key --populate archlinuxarm
```
```shell
sudo pacman -Syu sudo
```
Now type `visudo` and uncomment the wheel group.\
Note: If you'd like to use another text editor, like nano to configure sudo, then run `EDITOR=nano visudo`. Since your user is in the wheel group, it will have sudo priviledges.

Logout of root with exit and log back in with your newly created username and password. Test out sudo with `sudo pacman -Syu` to make sure everything works properly.

Arch Linux is now configured with your own user with sudo.

Optionally, you can install and configure ssh with the following:
```shell
sudo pacman -Syu openssh
```
```shell
sudo systemctl enable sshd
```
---
### Step 3: Create swap file

Use dd to create a swap file the size of your choosing. For example, creating a 1 GB swap file:
```shell
dd if=/dev/zero of=/mnt/Data/.swapfile bs=1M count=1024 status=progress
```
Set the right permissions (a world-readable swap file is a huge local vulnerability): 
```shell
chmod 600 /mnt/Data/.swapfile
```
After creating the correctly sized file, format it to swap: 
```shell
mkswap /mnt/Data/.swapfile
```
Activate the swap file:
```shell 
swapon /mnt/Data/.swapfile
```
Finally, edit the fstab configuration to add an entry for the swap file: 
```shell
sudo vim /etc/fstab
```
```shell
/mnt/Data/.swapfile none swap defaults 0 0
```
Use swap fiel location as UUID is not workin for the swap files, for the above, that was generetad when running mkswap 

---
### Step 4: Install and configure Kodi

Install kodi with
```shell
sudo pacman -Syu kodi-rpi-legacy
```
install yay and kodi-addon-inputstream-adaptive for Netflix to run on kodi
```shell
sudo pacman -S base-devel git
```
```shell
git clone https://aur.archlinux.org/yay.git
```
```shell
cd yay
```
```shell
makepkg -si
```
```shell
yay -S kodi-addon-inputstream-adaptive-git
```
Install Crypto packages and nss for Netflix:
```shell
sudo pacman -Syu python2-pycryptodome nss
```
Install Netflix from:
> https://github.com/CastagnaIT/plugin.video.netflix

Kodi will now run, but we need to do a bit of configuration to make sure video works properly.
Edit the `/boot/config.txt` file on your Raspberry Pi and change the `gpu_mem` tag like so:
```shell
gpu_mem=320
```
This will give the Raspberry Pi's GPU more memory on boot. (Which is needed for HD streaming).

---
### Step 4: Run Kodi!

Run kodi from your terminal (or SSH). If everything worked correctly, you should be able to stream videos. You can use the YouTube addon to test out if everything works. Then you can start adding your Movies or TV Shows to Kodi.

---
### Credits

Turorial is edited from adcar post following the comments in the post:
> https://gist.github.com/adcar/f2a61c942a878757719b3cb2bc233d38
