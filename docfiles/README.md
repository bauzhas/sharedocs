## Tutorial files

---
### Arch_install

Insatall instruction contain minimal Arch linux install.\
This install goes with:
1. syetemd
1. partitions `boot` `swap` `root` `home`
1. qtile windows manager
1. lightdm composer
1. disk LUKS encrypstion on root (60GB) and home partitions

Step 8, creating partitions needs polishing

Reference:
> jherrlin: https://jherrlin.github.io/posts/arch-install/ \
> OTB:      https://gitlab.com/OldTechBloke/dotfiles/-/blob/master/archinstall/archinstall.txt

---
### Alarm+Kodi+Rasp_Pi_3b+

Instructions contain Arch arm distro install on Raspberry Pi 3B+ with Kodi media center.\
This pretty much coppies adcars's tutorial + my edit on it following comments on the tutorial. I have added swap file to the installation and made a few more tweaks.

Reference:
> adcar: https://gist.github.com/adcar/f2a61c942a878757719b3cb2bc233d38

---
### Wireguard_set_up

A simple instruction on setting up wireguard client and server

Reference:
> https://www.the-digital-life.com/wireguard-installation-and-configuration/
