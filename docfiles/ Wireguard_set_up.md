### Install wireguard on server and the client (Alarm and Arch)
```shell
sudo pacman -S wireguard-ttols
```

### Generate public and private keys on the Server and the client
```shell
wg genkey | tee privatekey | wg pubkey > publickey
```
### Configure the server
```shell
sudo vim /etc/wireguard/wg0.conf
```
```shell
[Interface]
PrivateKey=<server-private-key>
Address=<server-ip-address>/<subnet>
SaveConfig=true
PostUp = iptables -A FORWARD -o wg0 -j ACCEPT; iptables -A FORWARD -i wg0 -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE;
PostDown = iptables -D FORWARD -o wg0 -j ACCEPT; iptables -D FORWARD -i wg0 -j ACCEPT; iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE;
ListenPort = 51820
```

### Configure client
```shell
sudo vim /etc/wireguard/wg0.conf
```

```shell
[Interface]
PrivateKey = <client-private-key>
Address = <client-ip-address>/<subnet>
SaveConfig = true

[Peer]
PublicKey = <server-public-key>
Endpoint = <server-public-ip-address>:51820
AllowedIPs = 0.0.0.0/0
PersistentKeepalive = 21
```

### Enable wireguard interface on Client
```shell
sudo wg-quick up wg0
```
Check wireguard status
```shell
sudo wg
```

### Add Client to Server
```shell
wg set wg0 peer <client-public-key> allowed-ips <client-ip-address>/32
```
Enable witreguard interface on Server
```shell
sudo wg-quick up wg0
```
Check wireguard status
```shell
sudo wg
```

---
### TROUBLESHOOTING
following belove link the rules in the server `wl0.conf` file were mnot allowing to access internet on the client side. I folloved https://www.reddit.com/r/WireGuard/comments/bkt4wl/connected_but_no_internet/ and changed `-i` to `-o` as per the advice. Tere is also comment on the-digital-life post about similar issue with the similar workaroud.

---
### Credits:
https://www.the-digital-life.com/wireguard-installation-and-configuration/
