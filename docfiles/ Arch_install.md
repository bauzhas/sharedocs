### Arch Linux installation with systemd and LUKS encryption

---

### Step 1

Download the Arch iso from:
https://www.archlinux.org/download/

---

### Step 2

1. Burn the iso to a CD/DVD or to a usb stick.  If you are coming from Windows I would recommend Rufus to burn to a usb stick. In Linux, `dd`{.shell} does the job well.
2. `dd` needs to be run as root and you should be very careful not to over-write the wrong drive.  The command, `sudo fdisk -l`, will list the drives connected to your system.
3. Issue the `fdisk` command after inserting the usb stick.  You are not interested in individual partitions on the device, just the device name itself, e.g. `/dev/sdd` not `/dev/sdd3`
4. `dd` is a really simple but effective tool.  It takes the input (`if=`) from one source (the iso in this case) and does a bite by bite copy to an output (`of=` ...the usb stick). So for a usb stick
-called `/dev/sdd` and an iso file saved in my Downloads directory, I would first cd to Downloads and then issue the following command:
```shell
sudo dd bs=4M if=<name of iso file> of=/dev/sdd status=progress oflag=sync
```
---

### Step 3

1. Boot the iso on the machine you want to install Arch to.  How you do this will vary from computer to computer, but I bring up a boot menu by hitting my F10 key.
2. Once you boot the process only takes a few seconds and you will find yourself at a root prompt.  No need to login, you can issue commands directly from the prompt.

---

### Step 4

1.    Check you are connected to the internet:
```shell
ping archlinux.org
```

2.    I would recommend you do this over a wired connection, but you can try to connect wirelessly using the following (as long as your drivers are on the iso):
```shell
wifi-menu
```
3.    If you are on a wired connection and still have no internet, check the name of the wired interface with ip a, mine is called eno1.  Then issue the following:
```shell
ip link set device eno1 up
```
---

### Step 5

1. The arch iso comes with ssh already installed and root logins are allowed.  You will need to set a password for root first though:
```shell
passwd
```
2. Then start the ssh service:
```shell
systemctl start sshd.service
```
3. Check the ip address of the arch machine:
```shell
ip a
```
4. Go to a terminal on the machine you want to use to do the install from and issue the following (change the ip accordingly)
```shell
ssh root@192.168.1.xxx
```
---

### Step 6

1. Set your keyboard layout.  US is the default so no need to do anything if that's what you want.  Otherwise, you can list the layouts available with the following command:
```shell
ls /usr/share/kbd/keymaps/i386/**/*.map.gz | less
```
2. Once you have found the keymap you want, and I use UK, set it as follows:
```shell
loadkeys uk.map.gz
```
3. I also set it up permanently in /etc/vconsole.conf
```shell
echo KEYMAP=uk > /etc/vconsole.conf
```
---

### Step 7

1. If you want to install a UEFI system, verify that you have booted the iso in the correct mode by issuing the following command:
```shell
ls /sys/firmware/efi/efivars
```
2. If the directory exists, you're good to go

---

### Step 8

1. Checking the system clock is accurate
```shell
timedatectl set-ntp true
```
2. Create Partitions by executing command
```shell
gdisk /dev/nvme0n1 
```
3. Remove all partitions.\
Create two new partitions: +1GB  EF00   EFI and boot\
Rest of disk: 8300   home and system
```shell
mkfs.fat -F32 /dev/nvmw0n1p1
```
4. Encrypt device
Create LUKS device
```shell
cryptsetup -v --use-random luksFormat /dev/nvmw0n1p1
```
5. Check the results
```shell
cryptsetup luksDump /dev/nvmw0n1p1
```
6. Create header backup
```shell
cryptsetup luksHeaderBackup /dev/nvme0n1p2 --header-backup-file luks_header_backup_\`date +%Y-%m-%d\`
```
7. Mount the device
```shell
cryptsetup open /dev/nvme0n1p1 Arch
```
8. Create file system on luks device
```shell
mkfs.ext4 /dev/mapper/Arch
```
9. Mount partitions
```shell
mount /dev/mapper/Arch /mnt
mkdir -p /mnt/boot
mount /dev/nvme1n1p1 /mnt/boot
```
---

### Step 9

1.    Install base packages 
```shell
pacstrap /mnt base base-devel linux linux-firmware vim less dialog wpa_supplicant dhcpcd sudo reflector pacman-contrib usbutils pciutils bash-completion git
```
2.    Create fstab
```shell
genfstab -U /mnt >> /mnt/etc/fstab  (Check creation with cat /mnt/etc/fstab)
```
3.    Chroot into the new system
```shell
arch-chroot /mnt /bin/bash
```
4.    Set the time Zone. Check available zones with `ls /usr/share/zoneinfo/` Mine is `Europe/London`, then create a symbolic link to `/etc/localtime`
```shell
ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
```
5.    Generate /etc/adjtime, assuming the hardware clock is set to utc
```shell
hwclock --systohc
```
6.    Set up localisation.  Find your locale by opening `/etc/locale.gen` and uncomment the entry you need.  Mine is `en_GB.UTF-8 UTF-8`. Alternatively:
```shell
echo en_GB.UTF-8 UTF-8 >> /etc/locale.gen
```
   - Then run
```shell
locale-gen
```
   - Then save your locale to `/etc/locale.conf`.  You don't need the last UTF-8, e.g.
```shell
echo LANG=en_GB.UTF-8 > /etc/locale.conf
```
   - Then export the locale
```shell
export LANG=en_GB.UTF-8
```
7.    Set up network and hosts. Decide on a hostname (I've chosen otbarch) and do this:
```shell
echo arch > /etc/hostname
```
Then edit your hosts file and insert the following (remember it's the host name you created in the last step). Use vim /etc/hosts
```
127.0.0.1 localhost\
::1		  localhost\
127.0.1.1 arch.localdomain arch
```
8.    Create a normal user. I'm calling mine otb
```shell
useradd -m -G sys,wheel,users,adm,log -s /bin/bash bauzhas
```
   1. Create a password
```shell
passwd bauzhas  (you will get asked to input it twice)
```
9.    Setup sudo
```shell
EDITOR=vim visudo	(Uncomment the line that states "Uncomment to allow members of group wheel to execute any command")
```
10.    Sync repos, optimise mirror list, and enable multilib. Uncomment the two lines that say `[multilib]` and `include = /etc/pacman.d/mirrorlist`
```shell
vim /etc/pacman.conf		
```
   10. Optimise mirrors. Change the entry below to represent your country:
```shell
reflector --country 'United Kingdom' --age 24 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
```
   10. Sync the repos
```shell
pacman -Syy	
```
---

### Step 10

1. Configure boot. Open mkinitcpio.conf:
```shell
vim /etc/mkinitcpio.conf
```
2. Add the following to each section:
```shell
MODULES=(ext4)
``` 
3. On `HOOKS` add `encrypt` before `filesystem`. Something like this:
```shell
HOOKS=(base udev autodetect modconf block encrypt filesystems keyboard fsck)
```
4. Create a new `initramfs` -not essential, but I always do it to check the kernel was installed properly
```shell
mkinitcpio -P linux
```
5. systemd boot configuration
```shell
bootctl --path=/boot install
```
```shell
echo 'default arch' >> /boot/loader/loader.conf\
```
```shell
echo 'timeout 3' >> /boot/loader/loader.conf
```
   5. Get the PARTUUID from the system partition into arch.conf
```shell
blkid -s PARTUUID -o value /dev/nvme0n1p1 >> /boot/loader/entries/arch.conf
```
   5. Open anch.conf for editing:
```shell
vim /boot/loader/entries/arch.conf
```
   5. Add the following content to `arch.conf`. The partition `<PARTUUID>` is already in the file.
```
title Arch Linux
linux /vmlinuz-linux
initrd /intel-ucode.img
initrd /initramfs-linux.img
options cryptdevice=PARTUUID=<PARTUUID>:Arch root=/dev/mapper/Arch rw
```

REBOOT if needed to the installed distro

---

### Step 11

1. Install and configure some essential packages
```shell
pacman -S xorg xorg-apps xorg-server xorg-drivers alacritty mesa linux-headers xdg-user-dirs fuse2 fuse3 ntfs-3g exfat-utils pulseaudio gvfs dkms haveged git unrar unzip htop lsb-release polkit man-db man-pages firefox
```
2. Install Network Manager, Printing, a Display Manager and some fonts
```shell
pacman -S cups system-config-printer foomatic-db-engine foomatic-db-ppds foomatic-db-nonfree-ppds gutenprint ghostscript networkmanager network-manager-applet lightdm lightdm-gtk-greeter ttf-ubuntu-font-family ttf-dejavu ttf-bitstream-vera ttf-liberation noto-fonts
```
3. Install qtile windows manager and dmenu
```shell
pacman -S qtile dmenu`
```
4. Enable netwmork manager service
```shell
systemctl enable NetworkManager
```
5. Enable lightdm and CUPS services
```shell
systemctl enable lightdm.service
```
```shell
systemctl enable org.cups.cupsd.service
```
---

### Step 12

1. Exit new system and go into the cd shell
```shell
exit
```
2. Unmount all
```shell
umount -R /mnt
```
3. Reboot system
```shell
shutdown -r now
```
4. Connect to WiFi
```shell
systemctl start NetworkManager
```
```shell
nmcli c add type wifi con-name "WiFi Name" ifname wlo1 ssid "WiFi Name"
```
```shell
nmcli c modify "WiFi Name" wifi-sec.key-mgmt wpa-psk wifi-sec.psk <password>
```
```shell
nmcli c up "WiFi Name"
```
5. Install microcode, either:
```shell
pacman -S intel-ucode
```
---

### Source:
jherrlin: https://jherrlin.github.io/posts/arch-install/ \
OTB:      https://gitlab.com/OldTechBloke/dotfiles/-/blob/master/archinstall/archinstall.txt
