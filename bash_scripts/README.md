#### surfshark-vpn.sh 

Dependencies:
- wget
- unzip
- openvpn\
> run `sudo pacman -Syyu wget unzip openvpn` if needed

Script made for Raspberry Pi Alarm (Arch Arm) distro to switch surfshark vpn servers and run them as services.\
Script chechks if there are any files with '*surfshark*.ovpn' in file  name and if not downloads Surfshark .ovpn files and configures the chosen connections. By default this is set to connect as tcp connections.\
To run it you would require to have Surfshark account.\
Script will request your root password to manage files in /etc/openvpn. It will request your surfshark account credentials for manaul set up.\

To prevent DNS leak:
- eddit /etc/resolve.conf file:
```
nameserver 162.252.172.57
nameserver 149.154.159.92
```

and add 
```
sudo chattr +i /etc/resolve.conf
```

I installed `update-systemd-resolved` from https://github.com/jonathanio/update-systemd-resolved and adding following lines to the bottom of /etc/nsswitch.conf file:
```
# Use systemd-resolved first, then fall back to /etc/resolv.conf
hosts: files resolve dns myhostname
# Use /etc/resolv.conf first, then fall back to systemd-resolved
hosts: files dns resolve myhostname
```
